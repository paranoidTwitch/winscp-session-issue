﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace winscp_troubleshooting
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                RunTest();

                Console.WriteLine("'q' to exit any other key to connect again.");
                ConsoleKeyInfo keyinfo = Console.ReadKey();

                if(keyinfo.Key == ConsoleKey.Q)
                {
                    return;
                }
            }
        }

        private static void RunTest()
        {
            SessionOptions options = new SessionOptions()
            {
                Protocol = Protocol.Ftp,
                HostName = "localhost",
                UserName = "test",
                Password = "12345"
            };

            using (Session session = new Session())
            {
                session.Open(options);

                TransferOptions transferOptions = new TransferOptions()
                {
                    TransferMode = TransferMode.Binary
                };

                Directory.CreateDirectory("testfiles");

                TransferOperationResult transferResult;
                transferResult = session.GetFiles(@"/", "/testfiles", false, transferOptions);

                transferResult.Check();

                foreach (TransferEventArgs transfer in transferResult.Transfers)
                {
                    Console.WriteLine("Download of {0} succeeded", transfer.FileName);
                }
            }
        }
    }
}
